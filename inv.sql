-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-05-2020 a las 07:59:26
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `inv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accion`
--

CREATE TABLE `accion` (
  `id` int(10) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `accion`
--

INSERT INTO `accion` (`id`, `nombre`) VALUES
(1, 'Producto creado'),
(2, 'Producto alterado'),
(3, 'Aumento de producto'),
(4, 'Disminucion de producto');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id` int(10) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`) VALUES
(1, 'alimentos'),
(2, 'limpieza'),
(3, 'bebidas'),
(4, 'frutas'),
(5, 'verduras');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `Categoria_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `nombre`, `descripcion`, `cantidad`, `Categoria_id`) VALUES
(1, 'pera', 'verde', 43, 4),
(2, 'manzana', 'roja', 13, 4),
(3, 'Coca cola', 'negra', 16, 3),
(4, 'omo', 'articulo de limpieza', 39, 2),
(5, 'arroz', 'blanco', 16, 1),
(6, 'tallarines', 'brochas', 17, 1),
(7, 'apio', 'verde', 17, 5),
(8, 'manzana', 'roja', 15, 4),
(9, 'tallarines', 'amarillo', 27, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro`
--

CREATE TABLE `registro` (
  `id` int(11) NOT NULL,
  `fecha` varchar(255) DEFAULT NULL,
  `Producto_id` int(11) NOT NULL,
  `Usuario_id` int(11) NOT NULL,
  `Accion_id` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `registro`
--

INSERT INTO `registro` (`id`, `fecha`, `Producto_id`, `Usuario_id`, `Accion_id`, `cantidad`) VALUES
(1, 'domingo, 24 de mayo de 2020, (17:53)', 1, 1, 1, 0),
(2, 'domingo, 24 de mayo de 2020, (17:53)', 1, 1, 3, 20),
(3, 'domingo, 24 de mayo de 2020, (17:53)', 1, 1, 4, 9),
(4, 'domingo, 24 de mayo de 2020, (17:54)', 2, 1, 1, 0),
(5, 'domingo, 24 de mayo de 2020, (17:57)', 3, 3, 1, 0),
(6, 'domingo, 24 de mayo de 2020, (20:29)', 4, 4, 1, 0),
(7, 'domingo, 24 de mayo de 2020, (20:29)', 4, 4, 3, 40),
(8, 'domingo, 24 de mayo de 2020, (20:30)', 4, 4, 4, 39),
(9, 'lunes, 25 de mayo de 2020, (16:07)', 5, 4, 1, 0),
(10, 'lunes, 25 de mayo de 2020, (16:10)', 1, 4, 3, 20),
(11, 'lunes, 25 de mayo de 2020, (16:10)', 6, 4, 1, 0),
(12, 'lunes, 25 de mayo de 2020, (16:12)', 1, 1, 3, 30),
(13, 'lunes, 25 de mayo de 2020, (16:20)', 1, 1, 3, 38),
(14, 'lunes, 25 de mayo de 2020, (16:21)', 7, 1, 1, 0),
(15, 'lunes, 25 de mayo de 2020, (16:43)', 1, 1, 3, 43),
(16, 'lunes, 25 de mayo de 2020, (16:43)', 8, 1, 1, 0),
(17, 'jueves, 28 de mayo de 2020, (1:47)', 9, 1, 1, 0),
(18, 'jueves, 28 de mayo de 2020, (1:47)', 9, 1, 3, 27);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(10) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `correo` varchar(255) DEFAULT NULL,
  `clave` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `correo`, `clave`, `estado`) VALUES
(1, 'Miguel Ortiz', 'm.ortiz09@ufromail.cl', '123123', 'activo'),
(3, 'Carlos Garrido', 'carlos123@gmail.cl', 'carlos123', 'noActivo'),
(4, 'Felipe Fernandez', 'pepeers@gmail.com', 'peper123', 'noActivo');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accion`
--
ALTER TABLE `accion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Producto_Categoria_idx` (`Categoria_id`);

--
-- Indices de la tabla `registro`
--
ALTER TABLE `registro`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Registro_Producto_idx` (`Producto_id`),
  ADD KEY `fk_Registro_Usuario_idx` (`Usuario_id`),
  ADD KEY `fk_Registro_Accion_idx` (`Accion_id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `accion`
--
ALTER TABLE `accion`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `registro`
--
ALTER TABLE `registro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `FKodqr7965ok9rwquj1utiamt0m` FOREIGN KEY (`Categoria_id`) REFERENCES `categoria` (`id`),
  ADD CONSTRAINT `fk_Producto_Categoria` FOREIGN KEY (`Categoria_id`) REFERENCES `categoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `registro`
--
ALTER TABLE `registro`
  ADD CONSTRAINT `FKkm2wrt1fbh382a4yxgc57purx` FOREIGN KEY (`Accion_id`) REFERENCES `accion` (`id`),
  ADD CONSTRAINT `FKkufda5haiij6q78uveuow5jrh` FOREIGN KEY (`Producto_id`) REFERENCES `producto` (`id`),
  ADD CONSTRAINT `FKqfbdwuu2isbwwnx1uky39930w` FOREIGN KEY (`Usuario_id`) REFERENCES `usuario` (`id`),
  ADD CONSTRAINT `fk_Registro_Accion` FOREIGN KEY (`Accion_id`) REFERENCES `accion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Registro_Producto` FOREIGN KEY (`Producto_id`) REFERENCES `producto` (`id`),
  ADD CONSTRAINT `fk_Registro_Usuario` FOREIGN KEY (`Usuario_id`) REFERENCES `usuario` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

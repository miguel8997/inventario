package cl.dci.ufro.inv;

import cl.dci.ufro.inv.dao.AccionDAO;
import cl.dci.ufro.inv.dao.CategoriaDAO;
import cl.dci.ufro.inv.dao.ProductoDAO;
import cl.dci.ufro.inv.dao.RegistroDAO;
import cl.dci.ufro.inv.dao.UsuarioDAO;
import cl.dci.ufro.inv.model.Categoria;
import cl.dci.ufro.inv.model.Fecha;
import cl.dci.ufro.inv.model.Producto;
import cl.dci.ufro.inv.model.Registro;
import cl.dci.ufro.inv.model.Usuario;
import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class InvApplicationTests {
    
    @Autowired
    private CategoriaDAO catDao;
    
    @Autowired
    private ProductoDAO proDao;
    
    @Autowired
    private AccionDAO accDao;
    
    @Autowired
    private UsuarioDAO usuDao;
    
    @Autowired
    private RegistroDAO regDao;
    
    
    private Producto p;
    
    @BeforeEach
    public void init() {
        p=new Producto();
        
    }
    
    @AfterEach
    public void finish() {
        p=null;
    }

    @Test
    @DisplayName("Agregar producto")
    void crearNuevoProducto() {
        
        List<Categoria> c=catDao.findAll();
        p.setNombre("cereal");
        p.setCantidad(12);
        p.setDescripcion("Chocapic");
        p.setCategoriaid(c.get(0));
        
        proDao.save(p);//Se agrega el producto a la base de datos
        List<Producto> productos=proDao.findAll();//Se trae todos los productos de la base de datos
        
        String valorEsperado = "cereal";
        String valorObtenido = productos.get(productos.size()-1).getNombre();//Nombre sacado de la BD
        
        assertEquals(valorEsperado, valorObtenido);  //Se compara el nombre del producto agregado,
                                                     //con el nombre del producto de la base de datos(BD)
        assertNotEquals("tallarines",valorObtenido);//Se compara un nombre cualquiera,
                                                     //con el nombre del producto de la base de datos(BD)
        assertNotNull(productos.get(productos.size()-1));//Se comprueba que el producto existe en la BD
    }
    
    @Test
    @DisplayName("Cambiar cantidad de producto")
    void cambiarCantidadProducto() {
        
        List<Producto> productos=proDao.findAll();//Se trae todos los productos de la base de datos
        p=proDao.findById(productos.get(productos.size()-1).getId()).orElse(null);//Se busca el producto por id
        p.setCantidad(30);//Se le cambia su cantidad
        proDao.save(p);//Se guarda el producto con su nueva cantidad
        List<Producto> productosBase=proDao.findAll();//Se trae todos los productos de la base de datos
        
        int valorEsperado = 30;
        int valorObtenido = productosBase.get(productos.size()-1).getCantidad();//Cantidad sacada de la BD
        
        assertEquals(valorEsperado, valorObtenido);//Se compara la cantidad del producto,
                                                   //con la cantidad del producto en la base de datos(BD)
                                                   
        assertNotEquals(12,valorObtenido);//Se compara una cantidad erronea,
                                          //con la cantidad del producto en la base de datos(BD)
                                          
        assertNotNull(productos.get(productos.size()-1));//Se comprueba que el producto existe en la BD
    }
    
    @Test
    @DisplayName("Usuarios registrados en la base de datos")
    void usuariosResgistrados() {
        
        List<Usuario> usuarios=usuDao.findAll();//Se trae todos los usuarios de la base de datos
        
        String valorEsperado = "m.ortiz09@ufromail.cl";
        String valorObtenido = usuarios.get(0).getCorreo();//Correo sacado desde la BD
        
        String valorEsperado2 = "123123";
        String valorObtenido2 = usuarios.get(0).getClave();//Clave sacada desde la BD
        
        assertEquals(valorEsperado, valorObtenido);//Se compara el carreo del usuario,
                                                   //con su correo de la base de datos
                                                   
        assertNotEquals("asjdadjk@gmail.cl",valorObtenido);//Se compara un correo erroneo,
                                                          //con el correo del usuario de la base de datos
                                                          
        assertEquals(valorEsperado2, valorObtenido2);//Se compara la clave del usuario,
                                                     //con la clave de la base de datos
                                                     
        assertNotEquals("324213",valorObtenido);//Se compara una clave erronea,
                                                //con la clave de la base de datos
    }

}

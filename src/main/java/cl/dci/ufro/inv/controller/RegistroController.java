
package cl.dci.ufro.inv.controller;

import cl.dci.ufro.inv.dao.RegistroDAO;
import cl.dci.ufro.inv.dao.UsuarioDAO;
import cl.dci.ufro.inv.model.Registro;
import cl.dci.ufro.inv.model.Usuario;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class RegistroController {
    
    @Autowired
    private RegistroDAO regDao;
    
    @Autowired
    private UsuarioDAO usuDao;
    
    @GetMapping("/registros")
    public String listarRegistros(Model model){
        
        String usuarioActivo="";
        List<Usuario> usuarios = usuDao.findAll();
        
        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i).getEstado().equalsIgnoreCase("activo")) {
                usuarioActivo=usuarios.get(i).getNombre();
            }
        }
        
        List<Registro> reg=regDao.findAll();
        
        model.addAttribute("registros", reg);
        model.addAttribute("usuarioActivo", usuarioActivo);
        return "registros";
    }
    
}

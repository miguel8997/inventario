package cl.dci.ufro.inv.controller;

import cl.dci.ufro.inv.dao.AccionDAO;
import cl.dci.ufro.inv.dao.CategoriaDAO;
import cl.dci.ufro.inv.dao.ProductoDAO;
import cl.dci.ufro.inv.dao.RegistroDAO;
import cl.dci.ufro.inv.dao.UsuarioDAO;
import cl.dci.ufro.inv.model.Accion;
import cl.dci.ufro.inv.model.Categoria;
import cl.dci.ufro.inv.model.Fecha;
import cl.dci.ufro.inv.model.Producto;
import cl.dci.ufro.inv.model.Registro;
import cl.dci.ufro.inv.model.Usuario;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class ProductoController {

    @Autowired
    private ProductoDAO productoDao;

    @Autowired
    private CategoriaDAO catDao;

    @Autowired
    private RegistroDAO regDao;

    @Autowired
    private AccionDAO accDao;

    @Autowired
    private UsuarioDAO usuDao;

    private int num;

    @GetMapping("/productos")
    public String muestraProductos(Model model) {
        String usuarioActivo="";
        
        List<Usuario> usuarios = usuDao.findAll();
        List<Producto> pro = productoDao.findAll();
        List<Categoria> categorias = new ArrayList<>();
        
        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i).getEstado().equalsIgnoreCase("activo")) {
                usuarioActivo=usuarios.get(i).getNombre();
            }
        }
        
        for (int i = 0; i < pro.size(); i++) {
            categorias.add(pro.get(i).getCategoriaid());
        }

        model.addAttribute("productos", pro);
        model.addAttribute("categorias", categorias);
        model.addAttribute("usuarioActivo", usuarioActivo);
        return "productos";
    }

    @GetMapping("/agregarProducto")
    public String agregar(Model model) {
        String usuarioActivo="";
        
        List<Usuario> usuarios = usuDao.findAll();
        List<Categoria> cat = catDao.findAll();
        Producto p = new Producto();
        
        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i).getEstado().equalsIgnoreCase("activo")) {
                usuarioActivo=usuarios.get(i).getNombre();
            }
        }

        model.addAttribute("categorias", cat);
        model.addAttribute("nuevoProducto", p);
        model.addAttribute("usuarioActivo", usuarioActivo);
        return "agregarProducto";
    }

    @PostMapping("/agregarProducto")
    public String guardar(@ModelAttribute Producto producto) throws IOException {

        productoDao.save(producto);

        Fecha f = new Fecha();
        String fecha = f.extraer();
        List<Accion> acciones = accDao.findAll();
        List<Usuario> usuarios = usuDao.findAll();

        Registro r = new Registro();
        r.setFecha(fecha);

        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i).getEstado().equalsIgnoreCase("activo")) {
                r.setUsuarioid(usuarios.get(i));
                break;
            }
        }

        System.out.println(this.num);
        System.out.println(producto.getCantidad());

        if (this.num == 0) {
            r.setCantidad(0);
            r.setProductoid(producto);
            r.setAccionid(acciones.get(0));
        } else if (this.num < producto.getCantidad()) {

            r.setCantidad(producto.getCantidad());
            r.setProductoid(producto);
            r.setAccionid(acciones.get(2));

        } else if (this.num > producto.getCantidad()) {

            r.setCantidad(producto.getCantidad());
            r.setProductoid(producto);
            r.setAccionid(acciones.get(3));
        }

        regDao.save(r);
        this.num=0;
        return "redirect:/productos";
    }

    @GetMapping("/editarCantidad/{id}")
    public String editar(@PathVariable("id") int idProducto, Model model) {
        String usuarioActivo="";
        
        List<Usuario> usuarios = usuDao.findAll();
        Producto p = productoDao.findById(idProducto).orElse(null);
        
        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i).getEstado().equalsIgnoreCase("activo")) {
                usuarioActivo=usuarios.get(i).getNombre();
            }
        }

        this.num = p.getCantidad();

        List<Categoria> cat = catDao.findAll();

        model.addAttribute("categorias", cat);
        model.addAttribute("cantidadEditar", p);
        model.addAttribute("usuarioActivo", usuarioActivo);

        return "editarCantidad";

    }
}

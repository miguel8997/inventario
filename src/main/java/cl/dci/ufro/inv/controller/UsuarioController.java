
package cl.dci.ufro.inv.controller;

import cl.dci.ufro.inv.dao.UsuarioDAO;
import cl.dci.ufro.inv.model.Usuario;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class UsuarioController {
    
    @Autowired
    private UsuarioDAO usuDao;
    
    @GetMapping({"/login","/home","/"})
    public String login(Model model){
        
        Usuario usu=new Usuario();
        
        
        model.addAttribute("usuario", usu);
        return "login";
    }
    
    @PostMapping("/login")
    public String verificarUsuario(@ModelAttribute Usuario usuario){
        boolean verificar=false;
        
        List<Usuario> usu=usuDao.findAll();
        
        for (int i = 0; i < usu.size(); i++) {
            if (usu.get(i).getCorreo().equalsIgnoreCase(usuario.getCorreo())) {
                verificar=true;
                break;
            }
        }
        
        if (verificar==true) {
            
            for (int i = 0; i < usu.size(); i++) {
                if (usu.get(i).getCorreo().equalsIgnoreCase(usuario.getCorreo())) {
                    usu.get(i).setEstado("activo");
                    usuDao.save(usu.get(i));

                } else {
                    usu.get(i).setEstado("noActivo");
                    usuDao.save(usu.get(i));

                }
            }
            
            return "redirect:/productos";
        }else{
            return "redirect:/login";
        }
        
    }
}

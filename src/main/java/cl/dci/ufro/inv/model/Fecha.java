
package cl.dci.ufro.inv.model;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Fecha {

    public Fecha() {
    }


    public String extraer() throws IOException {
        
        Document doc = Jsoup.connect("https://www.worldtimeserver.com/hora-exacta-CL.aspx").timeout(6000).get();
        Elements temp = doc.select("div.box-shadow");
        
        String hora=temp.select("span#theTime.fontTS").text();
        String fecha=temp.select("h4").text();
        String unir=fecha+", ("+hora+")";
        return unir;
    }

}


package cl.dci.ufro.inv.dao;

import cl.dci.ufro.inv.model.Producto;
import java.util.List;
import org.springframework.data.repository.CrudRepository;


public interface ProductoDAO extends CrudRepository<Producto,Integer> {

    @Override
    public List<Producto> findAll();
    
    
}

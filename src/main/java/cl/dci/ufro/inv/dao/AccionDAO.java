
package cl.dci.ufro.inv.dao;

import cl.dci.ufro.inv.model.Accion;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface AccionDAO extends CrudRepository<Accion,Integer> {
    
    @Override
    public List<Accion> findAll();
    
}

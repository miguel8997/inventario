/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.dci.ufro.inv.dao;

import cl.dci.ufro.inv.model.Usuario;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Sebastian Ortiz
 */
public interface UsuarioDAO extends CrudRepository<Usuario,Integer> {

    @Override
    public List<Usuario> findAll();
    
    
}

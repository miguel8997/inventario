/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.dci.ufro.inv.dao;

import cl.dci.ufro.inv.model.Categoria;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Sebastian Ortiz
 */
public interface CategoriaDAO extends CrudRepository<Categoria,Integer> {

    @Override
    public List<Categoria> findAll();
    
}
